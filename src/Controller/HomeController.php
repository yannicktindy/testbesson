<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home', methods: ['GET', 'POST'])]
    public function index(Request $request): Response
    {
        // Récupérer le contenu XML
        $xml = file_get_contents('build/Besson/client.xml');

        // Charger le contenu XML dans un objet simpleXML
        $data = simplexml_load_string($xml);

        // mise en tableau php de tous les fichier de données -------------------------------------------
        $clients = array();
        foreach ($data->Response->Object->ObjectClient as $client) {
            $clients[] = array(
                'code_postal' => (string) $client->codePostal,
                'id_client' => (int) $client->idClient,
                'raison_sociale' => (string) $client->raisonSociale,
                'ville' => (string) $client->ville,
            );
        }

        $xml = file_get_contents('build/Besson/conditiontaxation.xml');
        $data = simplexml_load_string($xml);
        $conditions = array();
        foreach ($data->Response->Object->ObjectConditionTaxation as $condition) {
            $conditions[] = array(
                'id_client' => (int) $condition->idClient,
                'taxe_port_du' => (float) $condition->taxePortDu,
                'taxe_port_paye' => (float) $condition->taxePortPaye,
                'use_taxe_port_du_generale' => (Boolean) $condition->useTaxePortDuGenerale,
                'use_taxe_port_paye_generale' => (Boolean) $condition->useTaxePortPayeGenerale,
            );
        }

        $xml = file_get_contents('build/Besson/tarif.xml');
        $data = simplexml_load_string($xml);
        $tarifs = array();
        foreach ($data->Response->Object->ObjectTarif as $tarif) {
            $tarifs[] = array(
                'code_departement' => (string) $tarif->codeDepartement,
                'id_client' => (int) $tarif->idClient,
                'id_cleint_heritage' => (int) $tarif->idCleintHeritage,
                'montant' => (float) $tarif->montant,
                'zone' => (int) $tarif->zone,
            );
        }

        $xml = file_get_contents('build/Besson/localite.xml');
        $data = simplexml_load_string($xml);
        $localites = array();
        foreach ($data->Response->Object->ObjectLocalite as $localite) {
            $localites[] = array(
                'code_postal' => (string) $localite->codePostal,
                'ville' => (string) $localite->ville,
                'zone' => (int) $localite->zone,
            );
        }

        // recuperation des données du formulaire --------------------------------------------------------
        // j'ai bien conscience que je sort de la logique objet mais je n'ai pas trouvé d'autre solution rapide 
        $expediteur =[];
        $destinataire =[];
        $quantitéColis=0;
        $reglement='';
        $zone = null;
        $departement='';
        $montant = 0;
        $taxe =0;
        if ($request->isMethod('POST')) {
            $expediteurId = $request->request->get('expediteurId');
            $destinataireId = $request->request->get('destinataireId');
            $reglement = $request->request->get('reglement');
            $quantitéColis = $request->request->get('colis');

            // je recupere le destinataire et l'expediteur en fonction de leur ID
            foreach($clients as $client){
                if($client['id_client'] == $expediteurId){
                    $expediteur = $client;
                }
                if($client['id_client'] == $destinataireId){
                    $destinataire = $client;
                }
            }
            // je recupere la zone, si pas de zone zone reste à null
            foreach($localites as $localite){
                if($localite['ville'] == $destinataire['ville']){
                    $zone = $localite['zone'];
                    $departement = $localite['code_postal'];
                }
            }

             // calcule du Montant           
            foreach($tarifs as $tarif){
                if($tarif['zone'] == $zone && $tarif['code_departement'] == $departement ){
                    if($tarif['id_client'] == $destinataire['id_client'] && $tarif['code_departement'] == $departement){
                        // si il y a un client heritage 
                        if($tarif['id_client_heritage']){
                            foreach($tarifs as $tarif){
                                if($tarif['id_client'] == $tarif['id_client_heritage'] && $tarif['code_departement'] == $departement){
                                    $montant = $tarif['montant'];
                                }
                            }
                            $montant = $tarif['montant'];
                        }  
                    }
                }
            }
            dump($departement);
            dump($montant);

        // calcule de la taxe
        if ($reglement  == 'expediteur') {
            foreach($conditions as $condition){
                if($condition['id_client'] == $expediteur['id_client']){
                    if($condition['use_taxe_port_du_generale'] == true){
                        $taxe = $condition['taxe_port_du'];
                    }else{
                        $taxe = $condition['taxe_port_du'];
                    }
                }
            }
        } else {
            foreach($conditions as $condition){
                if($condition['id_client'] == $destinataire['id_client']){
                    if($condition['use_taxe_port_paye_generale'] == true){
                        $taxe = $condition['taxe_port_paye'];
                    }else{
                        $taxe = $condition['taxe_port_paye'];
                    }
                }
            }

        } 






    }

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'clients' => $clients,
        ]);
    }
}